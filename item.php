<?php
/**
 * Created by PhpStorm.
 * User: sysadmin
 * Date: 1/4/20
 * Time: 11:55 AM
 */
require_once ('includes/meta.php');
?>
    <body>
        <div id="primary" class="container-fluid">

            <header id="header" class="row holderjs" data-background-src="?holder.js/1920x1080">
                <div class="col-12">

                </div>
            </header>
            <main role="main" class="row">
                <section class="offset-md-1 offset-lg-6 col-sm-12 col-md-10 col-lg-4">
                    <article>
                        <header class="row">
                            <h1 class="col page-title">Electra</h1>
                        </header>
                        <div id="product-images" class="row" style="margin-bottom:10px;">
                            <div class="col-3">
                                <img src="holder.js/75x75" alt="Thumbnail">
                            </div>
                            <div class="col-3">
                                <img src="holder.js/75x75" alt="Thumbnail">
                            </div>
                            <div class="col-3">
                                <img src="holder.js/75x75" alt="Thumbnail">
                            </div>
                        </div>
                        <div id="product-year" class="row">
                            <span class="col">2018</span>
                        </div>
                        <div id="product-collection" class="row">
                            <span class="col">Electra</span>
                        </div>
                        <div id="product-description" class="row">
                            <p class="col-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci commodi consequatur eos et hic inventore labore laborum, nostrum, obcaecati odit omnis possimus quaerat repellat, suscipit tenetur voluptas voluptatem voluptates?</p>
                            <p class="col-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci commodi consequatur eos et hic inventore labore laborum, nostrum, obcaecati odit omnis possimus quaerat repellat, suscipit tenetur voluptas voluptatem voluptates?</p>
                        </div>
                    </article>
                </section>
            </main>
            <?php include_once('includes/footer.php'); ?>
        </div>
        <?php require_once('includes/scripts.php'); ?>
    </body>
</html>