<?php
/**
 * Created by PhpStorm.
 * User: sysadmin
 * Date: 1/4/20
 * Time: 10:53 AM
 */
?>
<footer>
    <nav class="navbar fixed-bottom navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <span>Catie Curran Designs</span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav nav-fill w-100 align-items-center">
                <li class="nav-item active">
                    <a class="nav-link d-none d-lg-inline-block" href="#">Catie Curran Designs<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item d-inline-block d-lg-none">
                    <a class="nav-link" href="#">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">Gallery</a>
                </li>
                <li class="nav-item">
                    <a class="navbar-brand d-none d-lg-inline-block" href="#">
                        <!--<img style="width: 150px;" src="catiecurrandesigns_logo.png">-->
                        <!--<img src="holder.js/150x150">-->
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="page.php">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
                <li class="nav-item d-inline-block d-lg-none">
                    <a class="navbar-brand" href="#"></a>
                </li>
            </ul>
        </div>
    </nav>
</footer>