<?php
/**
 * Created by PhpStorm.
 * User: sysadmin
 * Date: 1/4/20
 * Time: 11:59 AM
 */
?>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <h1 class="page-title">EVERYDAY FANTASY JEWELRY AND ACCESSORIES</h1>
            <img class="d-block w-100" src="holder.js/1920x900" alt="First slide">
        </div>
        <div class="carousel-item">
            <h1 class="page-title">FANTASY JEWELRY FOR EVERY DAY WEAR</h1>
            <img class="d-block w-100" src="holder.js/1920x900" alt="Second slide">
        </div>
        <div class="carousel-item">
            <h1 class="page-title">Slide Three</h1>
            <img class="d-block w-100" src="holder.js/1920x900" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
