<?php
/**
 * Created by PhpStorm.
 * User: sysadmin
 * Date: 1/4/20
 * Time: 11:47 AM
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/sass/css/style.css">
    <title>Catie Curran Designs - Jeweler - Designer - Maker</title>
</head>
