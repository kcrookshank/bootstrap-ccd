<?php
/**
 * Created by PhpStorm.
 * User: sysadmin
 * Date: 1/4/20
 * Time: 10:54 AM
 */

require_once ('includes/meta.php');
?>
    <body>
        <div id="primary" class="container-fluid">

            <header id="header" class="row holderjs" data-background-src="?holder.js/1920x1080">
                <div class="col-12">
                    <?php include_once('includes/carousel.php'); ?>
                </div>
            </header>

            <?php include_once ('includes/footer.php'); ?>
        </div>
        <?php require_once ('includes/scripts.php'); ?>
    </body>
</html>