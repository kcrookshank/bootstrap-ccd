<?php
/**
 * Created by PhpStorm.
 * User: sysadmin
 * Date: 1/4/20
 * Time: 11:55 AM
 */
require_once ('includes/meta.php');
?>
    <body>
        <div id="primary" class="container-fluid">

            <header id="header" class="row holderjs" data-background-src="?holder.js/1920x1080">
                <div class="col-12">

                </div>
            </header>
            <main role="main" class="row">
                <section class="col-sm-12 col-md-12 col-lg-12">
                    <article>
                        <header class="row">
                            <h1 class="col page-title">Jewelery List</h1>
                        </header>
                        <div class="row">
                            <div class="col-4">
                                <img src="holder.js/75x75" alt="Thumbnail">
                            </div>
                            <div class="col-4">
                                <img src="holder.js/75x75" alt="Thumbnail">
                            </div>
                            <div class="col-4">
                                <img src="holder.js/75x75" alt="Thumbnail">
                            </div>
                        </div>
                    </article>
                </section>
            </main>
            <?php include_once('includes/footer.php'); ?>
        </div>
        <?php require_once('includes/scripts.php'); ?>
    </body>
</html>